const timer = {
  images: document.querySelectorAll(".image-to-show"),
  stopBtn: document.getElementById("stop"),
  continueBtn: document.getElementById("continue"),
  timerArea: document.getElementById("timer"),
  seconds: 3,
  currentImg: 1,
  previousImg: 0,
  start() {
    this.continueBtn.disabled = true;
    this.countdown();
    this.changeImages();
  },
  changeImages() {
    this.intervalId = setInterval(() => {
      this.images[this.currentImg].classList.replace("hide", "active");
      this.images[this.previousImg].classList.replace("active", "hide");
      this.previousImg = this.currentImg;

      // console.log(this.images[this.current]);
      if (this.currentImg < this.images.length - 1) {
        this.currentImg++;
      } else {
        this.currentImg = 0;
      }
    }, this.seconds * 1000);
  },
  stop() {
    clearInterval(this.intervalId);
    clearInterval(this.tick);
    this.timerArea.innerHTML = "";
    this.continueBtn.disabled = false;
  },
  countdown() {
    let timerArea = this.timerArea;
    let countdownSeconds = this.seconds;
    timerArea.innerHTML = countdownSeconds;
    
    this.tick = setInterval(function () {
      if (timerArea.innerHTML == 1) {
        countdownSeconds = 3;
        timerArea.innerHTML = countdownSeconds;
      } else {
        countdownSeconds -= 1;
        timerArea.innerHTML = countdownSeconds;
      }
    }, 1000);
  },
};

timer.start();
timer.stopBtn.addEventListener("click", timer.stop.bind(timer));
timer.continueBtn.addEventListener("click", timer.start.bind(timer));
